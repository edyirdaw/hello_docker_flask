# Dockerfile to demo docker with the awsome supervisord running in the background.
# sudo docker run -it -p 5000:5000 --name=flask1 my_docker_flask:latest sudo service supervisor start

FROM python:2.7

RUN echo 1 && apt-get update && apt-get install -y apt-utils sudo

RUN adduser --disabled-password --gecos "snetservice user" top && \
	adduser top sudo && \
	echo '%sudo ALL=(ALL) NOPASSWD:ALL' >> /etc/sudoers

USER top

COPY . /app

WORKDIR /app

RUN sudo pip install -r requirements.txt

RUN sudo apt-get install -y supervisor && \
    sudo rm /etc/supervisor/supervisord.conf

COPY supervisor/supervisord.conf /etc/supervisor/
COPY supervisor/hello_docker_flask.conf /etc/supervisor/conf.d/

RUN sudo mkdir /var/log/hello_docker_flask/

CMD ["sudo /usr/bin/supervisord"]
